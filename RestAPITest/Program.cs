﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace RestAPITest
{
    class Program
    {
        static void Main(string[] args)
        {
            //REST API call, Provides two values: Client Code (LHSC_CD) and Last Updated Date (2017-11-06)
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://helpdeskcrt.cerner.com/api/v2/chdi/LHSC_CD/tickets?lastUpdatedDate=2017-11-06");

            //Setup HTTP Request

            //Get Method used to grab tickets
            request.Method = "GET"; 
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

            //Authentication information
            request.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes("svcLHSC_CD:CT!s3rt18"));

            //Get HTTP Response, should return JSON
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            //Read JSON, place it in content variable
            string content = string.Empty;
            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader sr = new StreamReader(stream))
                {
                    content = sr.ReadToEnd();
                }
            }

            //Deserialize JSON into the Response Object.
            //The Response object contains a list of ticket objects. Because a response could contain multiple tickets, they are placed in a list.
            JavaScriptSerializer oJS = new JavaScriptSerializer();
            Response oRootObject = oJS.Deserialize<Response>(content);

            //Write ticket information to console
            Console.WriteLine("Last Modified Date: " + oRootObject.LastModifiedDate);
            Console.WriteLine("Ticket Count: " + oRootObject.Tickets.Count);

            foreach(Ticket ticket in oRootObject.Tickets)
            {
                Console.WriteLine("");
                Console.WriteLine("Ticket #" + (oRootObject.Tickets.IndexOf(ticket) + 1) + " - Ticket Type: " + ticket.TicketType);
                Console.WriteLine("Ticket #" + (oRootObject.Tickets.IndexOf(ticket) + 1) + " - Client Code: " + ticket.ClientCode);
                Console.WriteLine("Ticket #" + (oRootObject.Tickets.IndexOf(ticket) + 1) + " - Ticket Id: " + ticket.TicketId);
                Console.WriteLine("Ticket #" + (oRootObject.Tickets.IndexOf(ticket) + 1) + " - Viewable Ticket Id: " + ticket.ViewableTicketId);
            }

            Console.ReadLine();
        }

        public class Response
        {
            public string LastModifiedDate { get; set; }
            public List<Ticket> Tickets { get; set; }
        }

        public class Ticket
        {
            public string TicketType { get; set; }
            public string ClientCode { get; set; }
            public string TicketId { get; set; }
            public string ViewableTicketId { get; set; }
        }
    }
}
